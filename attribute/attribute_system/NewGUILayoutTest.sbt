<?xml version="1.0"?>
<!--Created by XmlV2StringWriter-->
<SMTK_AttributeSystem Version="2">
  <!--**********  Category and Analysis Information ***********-->
  <Categories>
    <Cat>Constituent</Cat>
    <Cat>Flow</Cat>
    <Cat>General</Cat>
    <Cat>Time</Cat>
  </Categories>
  <Analyses>
    <Analysis Type="CFD Flow">
      <Cat>Flow</Cat>
      <Cat>General</Cat>
      <Cat>Time</Cat>
    </Analysis>
    <Analysis Type="CFD Flow with Heat Transfer">
      <Cat>Flow</Cat>
      <Cat>General</Cat>
      <Cat>Heat</Cat>
      <Cat>Time</Cat>
    </Analysis>
    <Analysis Type="Constituent Transport">
      <Cat>Constituent</Cat>
      <Cat>General</Cat>
      <Cat>Time</Cat>
    </Analysis>
  </Analyses>
  <!--**********  Attribute Definitions ***********-->
  <Definitions>
    <AttDef Type="BoundaryCondition" BaseType="" Version="0" Abstract="true" Unique="true">
      <AssociationsDef Name="BoundaryConditionAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <MembershipMask>edge</MembershipMask>
      </AssociationsDef>
    </AttDef>
    <AttDef Type="VelocityBound" Label="Velocity BC" BaseType="BoundaryCondition" Version="0" Unique="true" Nodal="true">
      <AssociationsDef Name="VelocityBoundAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <MembershipMask>edge</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <Group Name="DirichletVelocity" Label="Dirichlet velocity:" Version="0" NumberOfRequiredGroups="1">
          <ItemDefinitions>
            <Double Name="Value1" Label="Value1" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Flow</Cat>
              </Categories>
              <ExpressionType>PolyLinearFunction</ExpressionType>
            </Double>
            <Double Name="Value2" Label="Value2" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Flow</Cat>
              </Categories>
              <ExpressionType>PolyLinearFunction</ExpressionType>
            </Double>
          </ItemDefinitions>
        </Group>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="VelocityAndDepthBound" Label="Velocity and Depth BC" BaseType="BoundaryCondition" Version="0" Unique="true" Nodal="true">
      <AssociationsDef Name="VelocityAndDepthBoundAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <MembershipMask>edge</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <Group Name="DirichletVelocityDepth" Label="Dirichlet velocity and depth:" Version="0" NumberOfRequiredGroups="1">
          <ItemDefinitions>
            <Double Name="Value1" Label="Value1" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Flow</Cat>
              </Categories>
              <ExpressionType>PolyLinearFunction</ExpressionType>
            </Double>
            <Double Name="Value2" Label="Value2" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Flow</Cat>
              </Categories>
              <ExpressionType>PolyLinearFunction</ExpressionType>
            </Double>
            <Double Name="Value3" Label="Value3" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Flow</Cat>
              </Categories>
              <ExpressionType>PolyLinearFunction</ExpressionType>
            </Double>
          </ItemDefinitions>
        </Group>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="LidElevation" Label="Lid Elevation BC" BaseType="BoundaryCondition" Version="0" Unique="true" Nodal="true">
      <AssociationsDef Name="LidElevationAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <MembershipMask>edge</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <Double Name="Value" Label="Stationary lid elevation:" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Flow</Cat>
          </Categories>
          <ExpressionType>PolyLinearFunction</ExpressionType>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="WaterDepthLid" Label="Water Depth Lid BC" BaseType="BoundaryCondition" Version="0" Unique="true" Nodal="true">
      <AssociationsDef Name="WaterDepthLidAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <MembershipMask>edge</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <Double Name="Value" Label="Water depth under stationary lid:" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Flow</Cat>
          </Categories>
          <ExpressionType>PolyLinearFunction</ExpressionType>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="FloatingStationary" Label="Floating Stationary BC" BaseType="BoundaryCondition" Version="0" Unique="true" Nodal="true">
      <AssociationsDef Name="FloatingStationaryAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <MembershipMask>edge</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <Double Name="Value" Label="Floating stationary object:" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Flow</Cat>
          </Categories>
          <ExpressionType>PolyLinearFunction</ExpressionType>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="TotalDischarge" Label="Total Discharge BC" BaseType="BoundaryCondition" Version="0" Unique="true" Nodal="true">
      <AssociationsDef Name="TotalDischargeAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <MembershipMask>edge</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <Double Name="Value" Label="Total discharge:" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Flow</Cat>
          </Categories>
          <ExpressionType>PolyLinearFunction</ExpressionType>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="UnitFlow" Label="Unit Flow BC" BaseType="BoundaryCondition" Version="0" Unique="true">
      <AssociationsDef Name="UnitFlowAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <MembershipMask>edge</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <Double Name="Value" Label="Unit flow:" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Flow</Cat>
          </Categories>
          <ExpressionType>PolyLinearFunction</ExpressionType>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="WaterSurfElev" Label="Water Surf Elev BC" BaseType="BoundaryCondition" Version="0" Unique="true">
      <AssociationsDef Name="WaterSurfElevAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <MembershipMask>edge</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <Double Name="Value" Label="Water surface elevation:" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Flow</Cat>
          </Categories>
          <ExpressionType>PolyLinearFunction</ExpressionType>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="TransportBoundaryCondition" BaseType="BoundaryCondition" Version="0" Abstract="true" Unique="false">
      <AssociationsDef Name="TransportBoundaryConditionAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <MembershipMask>edge</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <AttributeRef Name="Constituent" Label="Constituent" Version="0" NumberOfRequiredValues="1">
          <AttDef>Constituent</AttDef>
          <ComponentLabels>
            <Label />
          </ComponentLabels>
        </AttributeRef>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="DirichletTransport" Label="Dirichlet transport:" BaseType="TransportBoundaryCondition" Version="0" Unique="true" Nodal="true">
      <AssociationsDef Name="DirichletTransportAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <MembershipMask>edge</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <Double Name="Value" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Constituent</Cat>
          </Categories>
          <ExpressionType>PolyLinearFunction</ExpressionType>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="NaturalTransport" Label="Natural transport:" BaseType="TransportBoundaryCondition" Version="0" Unique="true">
      <AssociationsDef Name="NaturalTransportAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <MembershipMask>edge</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <Double Name="Value" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Constituent</Cat>
          </Categories>
          <ExpressionType>PolyLinearFunction</ExpressionType>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="Constituent" Label="Constituent" BaseType="" Version="0" Abstract="true" Unique="true" />
    <AttDef Type="VorticityConstituent" Label="Vorticity Constituent" BaseType="Constituent" Version="0" Unique="true">
      <ItemDefinitions>
        <Group Name="VortConstituentParams" Label="Vorticity constituent transport parameters" Version="0" NumberOfRequiredGroups="1">
          <ItemDefinitions>
            <Double Name="NormalizationFactor" Label="Normalization Factor" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Constituent</Cat>
              </Categories>
              <DefaultValue>1</DefaultValue>
              <RangeInfo>
                <Min Inclusive="false">0</Min>
              </RangeInfo>
            </Double>
            <Double Name="AsTerm" Label="As Term" Version="0" NumberOfRequiredValues="1">
              <DefaultValue>0</DefaultValue>
              <RangeInfo>
                <Min Inclusive="true">0</Min>
              </RangeInfo>
            </Double>
            <Double Name="DsTerm" Label="Ds Term" Version="0" NumberOfRequiredValues="1">
              <DefaultValue>0</DefaultValue>
              <RangeInfo>
                <Min Inclusive="true">0</Min>
              </RangeInfo>
            </Double>
          </ItemDefinitions>
        </Group>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="SalinityConstituent" Label="Salinity Constituent" BaseType="Constituent" Version="0" Unique="true">
      <ItemDefinitions>
        <Group Name="SalConstituentParams" Label="Salinity transport parameters:" Version="0" NumberOfRequiredGroups="1">
          <ItemDefinitions>
            <Double Name="ReferenceConcentration" Label="Reference Concentration" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Constituent</Cat>
              </Categories>
              <DefaultValue>1</DefaultValue>
              <RangeInfo>
                <Min Inclusive="false">0</Min>
              </RangeInfo>
            </Double>
          </ItemDefinitions>
        </Group>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="TemperatureConstituent" Label="Temperature Constituent" BaseType="Constituent" Version="0" Unique="true">
      <ItemDefinitions>
        <Group Name="TempConstituentParams" Label="Temperature transport parameters:" Version="0" NumberOfRequiredGroups="1">
          <ItemDefinitions>
            <Double Name="ReferenceConcentration" Label="Reference Concentration" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Constituent</Cat>
              </Categories>
              <DefaultValue>1</DefaultValue>
              <RangeInfo>
                <Min Inclusive="false">0</Min>
              </RangeInfo>
            </Double>
          </ItemDefinitions>
        </Group>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="GeneralConstituent" Label="General Constituent" BaseType="Constituent" Version="0" Unique="true">
      <ItemDefinitions>
        <Group Name="GenConstituentParams" Label="General transport parameters" Version="0" NumberOfRequiredGroups="1">
          <ItemDefinitions>
            <Double Name="ReferenceConcentration" Label="Characteristic Concentration" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Constituent</Cat>
              </Categories>
              <DefaultValue>1</DefaultValue>
              <RangeInfo>
                <Min Inclusive="false">0</Min>
              </RangeInfo>
            </Double>
          </ItemDefinitions>
        </Group>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="Friction" Label="Friction" BaseType="" Version="0" Abstract="true" Unique="true">
      <AssociationsDef Name="FrictionAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <MembershipMask>face</MembershipMask>
      </AssociationsDef>
    </AttDef>
    <AttDef Type="FrictionType1" Label="Type 1 (MNG) Friction:" BaseType="Friction" Version="0" Unique="true">
      <AssociationsDef Name="FrictionType1Associations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <MembershipMask>face</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <Double Name="Value" Label="MNG" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>General</Cat>
          </Categories>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="FrictionType2" Label="Type 2 (ERH) Friction:" BaseType="Friction" Version="0" Unique="true">
      <AssociationsDef Name="FrictionType2Associations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <MembershipMask>face</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <Double Name="Value" Label="ERH" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>General</Cat>
          </Categories>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="FrictionType3" Label="Type 3 (SAV) Friction:" BaseType="Friction" Version="0" Unique="true">
      <AssociationsDef Name="FrictionType3Associations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <MembershipMask>face</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <Double Name="Value" Label="SAV" Version="0" NumberOfRequiredValues="2">
          <Categories>
            <Cat>General</Cat>
          </Categories>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="FrictionType4" Label="Type 4 (URV) Friction:" BaseType="Friction" Version="0" Unique="true">
      <AssociationsDef Name="FrictionType4Associations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <MembershipMask>face</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <Double Name="Value" Label="URV" Version="0" NumberOfRequiredValues="3">
          <Categories>
            <Cat>General</Cat>
          </Categories>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="Globals" Label="Globals" BaseType="" Version="0" Unique="true">
      <ItemDefinitions>
        <Double Name="Gravity" Label="Gravity:" Version="0" NumberOfRequiredValues="1" Units="m/s^2">
          <Categories>
            <Cat>General</Cat>
          </Categories>
          <DefaultValue>9.8000000000000007</DefaultValue>
          <RangeInfo>
            <Min Inclusive="false">0</Min>
          </RangeInfo>
        </Double>
        <Double Name="KinMolViscosity" Label="Kinematic molecular viscosity:" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>General</Cat>
          </Categories>
          <DefaultValue>9.9999999999999995e-07</DefaultValue>
          <RangeInfo>
            <Min Inclusive="false">0</Min>
          </RangeInfo>
        </Double>
        <Double Name="ReferenceDensity" Label="Fluid density:" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>General</Cat>
          </Categories>
          <DefaultValue>1000</DefaultValue>
          <RangeInfo>
            <Min Inclusive="false">0</Min>
          </RangeInfo>
        </Double>
        <Double Name="ManningsUnitConstant" Label="Manning's unit constant (1.0 for SI, 1.486 for English):" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>General</Cat>
          </Categories>
          <DefaultValue>1</DefaultValue>
          <RangeInfo>
            <Min Inclusive="false">0</Min>
          </RangeInfo>
        </Double>
        <Double Name="WetDryLimits" Label="Wetting/drying limits:" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>General</Cat>
          </Categories>
          <DefaultValue>0</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">0</Min>
          </RangeInfo>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="SimExpression" BaseType="" Version="0" Abstract="true" Unique="true" />
    <AttDef Type="SimInterpolation" BaseType="SimExpression" Version="0" Abstract="true" Unique="true" />
    <AttDef Type="PolyLinearFunction" Label="PolyLinear Function" BaseType="SimInterpolation" Version="0" Unique="true">
      <ItemDefinitions>
        <Group Name="ValuePairs" Label="Value Pairs" Version="0" NumberOfRequiredGroups="1">
          <ItemDefinitions>
            <Double Name="X" Version="0" NumberOfRequiredValues="0" Extensible="true" />
            <Double Name="Value" Version="0" NumberOfRequiredValues="0" Extensible="true" />
          </ItemDefinitions>
        </Group>
        <String Name="Sim1DLinearExp" Version="0" NumberOfRequiredValues="1" />
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="SolidMaterial" Label="Solid Material" BaseType="" Version="0" Unique="true">
      <AssociationsDef Name="SolidMaterialAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <MembershipMask>face</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <Group Name="TurbulentDiffusionRate" Label="Turbulent diffusion rate" Version="0" NumberOfRequiredGroups="1">
          <ItemDefinitions>
            <Double Name="DefaultValue" Label="Default turbulent diffusion rate" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Constituent</Cat>
              </Categories>
              <BriefDescription>The default value applied to any constituents not assigned individually.</BriefDescription>
              <DefaultValue>9.9999999999999995e-07</DefaultValue>
              <RangeInfo>
                <Min Inclusive="false">0</Min>
              </RangeInfo>
            </Double>
            <Group Name="IndividualValue" Label="Individual turbulent diffusion rates" Version="0" NumberOfRequiredGroups="0" Extensible="true">
              <BriefDescription>The value for one individual constituent.</BriefDescription>
              <ComponentLabels CommonLabel="" />
              <ItemDefinitions>
                <AttributeRef Name="Constituent" Label="Constituent" Version="0" NumberOfRequiredValues="1">
                  <AttDef>Constituent</AttDef>
                  <ComponentLabels>
                    <Label />
                  </ComponentLabels>
                </AttributeRef>
                <Double Name="Value" Label="Turbulent diffusion rate" Version="0" NumberOfRequiredValues="1">
                  <Categories>
                    <Cat>Constituent</Cat>
                  </Categories>
                  <DefaultValue>9.9999999999999995e-07</DefaultValue>
                  <RangeInfo>
                    <Min Inclusive="false">0</Min>
                  </RangeInfo>
                </Double>
              </ItemDefinitions>
            </Group>
          </ItemDefinitions>
        </Group>
        <Group Name="KinematicEddyViscosity" Label="Kinematic eddy viscosity:" Version="0" NumberOfRequiredGroups="1">
          <ItemDefinitions>
            <Double Name="Value1" Label="Exx" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>General</Cat>
              </Categories>
              <DefaultValue>0.5</DefaultValue>
              <RangeInfo>
                <Min Inclusive="false">0</Min>
              </RangeInfo>
            </Double>
            <Double Name="Value2" Label="Eyy" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>General</Cat>
              </Categories>
              <DefaultValue>0.5</DefaultValue>
              <RangeInfo>
                <Min Inclusive="false">0</Min>
              </RangeInfo>
            </Double>
            <Double Name="Value3" Label="Exy" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>General</Cat>
              </Categories>
              <DefaultValue>0.5</DefaultValue>
              <RangeInfo>
                <Min Inclusive="false">0</Min>
              </RangeInfo>
            </Double>
          </ItemDefinitions>
        </Group>
        <Double Name="CoriolisLatitude" Label="Coriolis Latitude:" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>General</Cat>
          </Categories>
          <DefaultValue>0</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">-90</Min>
            <Max Inclusive="true">90</Max>
          </RangeInfo>
        </Double>
        <Int Name="MaxRefineLevels" Label="Maximum Levels of Refinement:" Version="0" AdvanceLevel="1" NumberOfRequiredValues="1">
          <Categories>
            <Cat>General</Cat>
          </Categories>
          <DefaultValue>0</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">0</Min>
          </RangeInfo>
        </Int>
        <Double Name="HydroRefineTol" Label="Hydro refinement tolerance:" Version="0" AdvanceLevel="1" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Flow</Cat>
          </Categories>
          <DefaultValue>1</DefaultValue>
          <RangeInfo>
            <Min Inclusive="false">0</Min>
          </RangeInfo>
        </Double>
        <Group Name="TransportRefineTol" Label="Transport refinement tolerance:" Version="0" Optional="true" IsEnabledByDefault="false" AdvanceLevel="1" NumberOfRequiredGroups="1">
          <ItemDefinitions>
            <Double Name="DefaultValue" Label="Default transport refinement tolerance" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Constituent</Cat>
              </Categories>
              <BriefDescription>The default value applied to any constituents not assigned individually.</BriefDescription>
              <DefaultValue>1</DefaultValue>
              <RangeInfo>
                <Min Inclusive="false">0</Min>
              </RangeInfo>
            </Double>
            <Group Name="IndividualValue" Label="Individual transport refinement tolerances" Version="0" NumberOfRequiredGroups="0" Extensible="true">
              <BriefDescription>The value for one individual constituent.</BriefDescription>
              <ComponentLabels CommonLabel="" />
              <ItemDefinitions>
                <AttributeRef Name="Constituent" Label="Constituent" Version="0" NumberOfRequiredValues="1">
                  <AttDef>Constituent</AttDef>
                  <ComponentLabels>
                    <Label />
                  </ComponentLabels>
                </AttributeRef>
                <Double Name="Value" Label="Transport refinement tolerance" Version="0" NumberOfRequiredValues="1">
                  <Categories>
                    <Cat>Constituent</Cat>
                  </Categories>
                  <DefaultValue>1</DefaultValue>
                  <RangeInfo>
                    <Min Inclusive="false">0</Min>
                  </RangeInfo>
                </Double>
              </ItemDefinitions>
            </Group>
          </ItemDefinitions>
        </Group>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="Solvers" Label="Solvers" BaseType="" Version="0" Unique="true">
      <ItemDefinitions>
        <Int Name="MemoryIncrementBlockSize" Label="Block size for memory increment:" Version="0" AdvanceLevel="1" NumberOfRequiredValues="1">
          <Categories>
            <Cat>General</Cat>
          </Categories>
          <DefaultValue>40</DefaultValue>
          <RangeInfo>
            <Min Inclusive="false">0</Min>
          </RangeInfo>
        </Int>
        <Int Name="PreconditioningBlocks" Label="Subdomain blocks per processor for preconditioning:" Version="0" AdvanceLevel="1" NumberOfRequiredValues="1">
          <Categories>
            <Cat>General</Cat>
          </Categories>
          <DefaultValue>1</DefaultValue>
          <RangeInfo>
            <Min Inclusive="false">0</Min>
          </RangeInfo>
        </Int>
        <Int Name="PreconditionerType" Label="Preconditioner Type:" Version="0" AdvanceLevel="1" NumberOfRequiredValues="1">
          <Categories>
            <Cat>General</Cat>
          </Categories>
          <DiscreteInfo DefaultIndex="0">
            <Value Enum="None">0</Value>
            <Value Enum="1 Level Additive Schwartz">1</Value>
            <Value Enum="2 Level Additive Schwartz">2</Value>
            <Value Enum="2 Level Hybrid">3</Value>
          </DiscreteInfo>
        </Int>
        <Double Name="TemporalSchemeCoefficient" Label="Coefficient for the second order temporal scheme:" Version="0" Optional="true" IsEnabledByDefault="false" AdvanceLevel="1" NumberOfRequiredValues="1">
          <Categories>
            <Cat>General</Cat>
          </Categories>
          <DefaultValue>0</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">0</Min>
            <Max Inclusive="true">1</Max>
          </RangeInfo>
        </Double>
        <Double Name="PetrovGalerkinCoefficient" Label="Coefficient for the Petrov-Galerkin equation:" Version="0" Optional="true" IsEnabledByDefault="false" AdvanceLevel="1" NumberOfRequiredValues="1">
          <Categories>
            <Cat>General</Cat>
          </Categories>
          <DefaultValue>0</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">0</Min>
            <Max Inclusive="true">0.5</Max>
          </RangeInfo>
        </Double>
        <Void Name="VesselMovement" Label="Enable vessel movement:" Version="0" Optional="true" IsEnabledByDefault="false">
          <Categories>
            <Cat>General</Cat>
          </Categories>
        </Void>
        <Void Name="VesselEntrainment" Label="Enable vessel entrainment:" Version="0" Optional="true" IsEnabledByDefault="false">
          <Categories>
            <Cat>General</Cat>
          </Categories>
        </Void>
        <Void Name="SW2Gradients" Label="Compute SW2 gradients:" Version="0" Optional="true" IsEnabledByDefault="false" AdvanceLevel="1">
          <Categories>
            <Cat>General</Cat>
          </Categories>
        </Void>
        <Double Name="NonLinearTolMaxNorm" Label="Non-Linear Tolerance (Max Norm):" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>General</Cat>
          </Categories>
          <DefaultValue>0.0001</DefaultValue>
          <RangeInfo>
            <Min Inclusive="false">0</Min>
          </RangeInfo>
        </Double>
        <Double Name="NonLinearTolMaxChange" Label="Non-Linear Tolerance (Maximum Change in Iteration):" Version="0" Optional="true" IsEnabledByDefault="false" AdvanceLevel="1" NumberOfRequiredValues="1">
          <Categories>
            <Cat>General</Cat>
          </Categories>
          <DefaultValue>0.01</DefaultValue>
          <RangeInfo>
            <Min Inclusive="false">0</Min>
          </RangeInfo>
        </Double>
        <Int Name="MaxNonLinearIters" Label="Maximum nonlinear iterations per time step:" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>General</Cat>
          </Categories>
          <DefaultValue>6</DefaultValue>
          <RangeInfo>
            <Min Inclusive="false">0</Min>
          </RangeInfo>
        </Int>
        <Int Name="MaxLinearIters" Label="Maximum linear iterations per nonlinear iteration:" Version="0" Optional="true" IsEnabledByDefault="false" AdvanceLevel="1" NumberOfRequiredValues="1">
          <Categories>
            <Cat>General</Cat>
          </Categories>
          <DefaultValue>200</DefaultValue>
          <RangeInfo>
            <Min Inclusive="false">0</Min>
          </RangeInfo>
        </Int>
        <Void Name="RungeKuttaTol" Label="Runge-Kutta tolerance for reactive constituents:" Version="0" Optional="true" IsEnabledByDefault="false" AdvanceLevel="1">
          <Categories>
            <Cat>General</Cat>
          </Categories>
        </Void>
        <Double Name="HydrodynamicsTol" Label="Quasi-unsteady tolerance for hydrodynamics:" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>General</Cat>
          </Categories>
          <DefaultValue>0.0001</DefaultValue>
          <RangeInfo>
            <Min Inclusive="false">0</Min>
          </RangeInfo>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="Time" Label="Time" BaseType="" Version="0" Unique="true">
      <ItemDefinitions>
        <Double Name="TimestepSize" Label="Timestep size:" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>General</Cat>
          </Categories>
          <ExpressionType>PolyLinearFunction</ExpressionType>
        </Double>
        <Group Name="StartTime" Label="Start Time:" Version="0" NumberOfRequiredGroups="1">
          <ItemDefinitions>
            <Double Name="Value" Label="Value" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>General</Cat>
              </Categories>
              <DefaultValue>0</DefaultValue>
              <RangeInfo>
                <Min Inclusive="true">0</Min>
              </RangeInfo>
            </Double>
            <Int Name="Units" Label="Units" Version="0" NumberOfRequiredValues="1">
              <DiscreteInfo DefaultIndex="0">
                <Value Enum="Seconds">0</Value>
                <Value Enum="Minutes">1</Value>
                <Value Enum="Hours">2</Value>
                <Value Enum="Days">3</Value>
                <Value Enum="Weeks">4</Value>
              </DiscreteInfo>
            </Int>
          </ItemDefinitions>
        </Group>
        <Group Name="EndTime" Label="End Time:" Version="0" NumberOfRequiredGroups="1">
          <ItemDefinitions>
            <Double Name="Value" Label="Value" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>General</Cat>
              </Categories>
              <DefaultValue>0</DefaultValue>
            </Double>
            <Int Name="Units" Label="Units" Version="0" NumberOfRequiredValues="1">
              <DiscreteInfo DefaultIndex="0">
                <Value Enum="Seconds">0</Value>
                <Value Enum="Minutes">1</Value>
                <Value Enum="Hours">2</Value>
                <Value Enum="Days">3</Value>
                <Value Enum="Weeks">4</Value>
              </DiscreteInfo>
            </Int>
          </ItemDefinitions>
        </Group>
        <Double Name="SteadyStateSolveParams" Label="Steady state solve parameters:" Version="0" Optional="true" IsEnabledByDefault="false" NumberOfRequiredValues="1">
          <Categories>
            <Cat>General</Cat>
          </Categories>
          <RangeInfo>
            <Min Inclusive="false">0</Min>
          </RangeInfo>
        </Double>
        <Group Name="QuasiUnsteadyParams" Label="Parameters defining quasi-unsteady simulations:" Version="0" Optional="true" IsEnabledByDefault="false" NumberOfRequiredGroups="1">
          <ItemDefinitions>
            <Double Name="SteadyStateHydrodynamicCondition" Label="Steady State Hydrodynamic Condition" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>General</Cat>
              </Categories>
              <BriefDescription>Select Function</BriefDescription>
              <ExpressionType>PolyLinearFunction</ExpressionType>
            </Double>
            <Int Name="MaxIterations" Label="Max iterations for steady state solve" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>General</Cat>
              </Categories>
              <RangeInfo>
                <Min Inclusive="false">0</Min>
              </RangeInfo>
            </Int>
            <Double Name="InitialTimeStep" Label="Initial time step for steady state calculation" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>General</Cat>
              </Categories>
              <RangeInfo>
                <Min Inclusive="false">0</Min>
              </RangeInfo>
            </Double>
          </ItemDefinitions>
        </Group>
        <Group Name="AutoTimeStepFind" Label="Automatic timestep find:" Version="0" Optional="true" IsEnabledByDefault="false" NumberOfRequiredGroups="1">
          <ItemDefinitions>
            <Double Name="InitialTimeStep" Label="Initial Time Step Size" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>General</Cat>
              </Categories>
              <RangeInfo>
                <Min Inclusive="false">0</Min>
              </RangeInfo>
            </Double>
            <Double Name="TimeSeries" Label="Time Series" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>General</Cat>
              </Categories>
              <ExpressionType>PolyLinearFunction</ExpressionType>
            </Double>
          </ItemDefinitions>
        </Group>
        <Void Name="PrintAdaptedMeshes" Label="Print adapted meshes:" Version="0" Optional="true" IsEnabledByDefault="false">
          <Categories>
            <Cat>General</Cat>
          </Categories>
        </Void>
        <Int Name="HotStartFile" Label="Hotstart File" Version="0" Optional="true" IsEnabledByDefault="false" NumberOfRequiredValues="1">
          <RangeInfo>
            <Min Inclusive="false">0</Min>
          </RangeInfo>
        </Int>
        <Int Name="OutputSeries" Label="Output Series" Version="0" Optional="true" IsEnabledByDefault="false" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Time</Cat>
          </Categories>
          <ChildrenDefinitions>
            <Double Name="OutputFunction" Label="Output Function" Version="0" NumberOfRequiredValues="1">
              <ExpressionType>PolyLinearFunction</ExpressionType>
            </Double>
            <Int Name="OutputUnits" Label="Output Units" Version="0" NumberOfRequiredValues="1">
              <DiscreteInfo DefaultIndex="0">
                <Value Enum="Seconds">0</Value>
                <Value Enum="Minutes">1</Value>
                <Value Enum="Hours">2</Value>
                <Value Enum="Days">3</Value>
                <Value Enum="Weeks">4</Value>
              </DiscreteInfo>
            </Int>
            <Group Name="TimeSeriesData" Label="Series" Version="0" NumberOfRequiredGroups="1" Extensible="true">
              <ComponentLabels CommonLabel="" />
              <ItemDefinitions>
                <Double Name="StartTime" Label="Start Time" Version="0" NumberOfRequiredValues="1">
                  <RangeInfo>
                    <Min Inclusive="true">0</Min>
                  </RangeInfo>
                </Double>
                <Double Name="EndTime" Label="End Time" Version="0" NumberOfRequiredValues="1">
                  <RangeInfo>
                    <Min Inclusive="false">0</Min>
                  </RangeInfo>
                </Double>
                <Double Name="TimeInterval" Label="Progression Interval" Version="0" NumberOfRequiredValues="1" />
                <Int Name="Units" Label="Input Units" Version="0" NumberOfRequiredValues="1">
                  <DiscreteInfo DefaultIndex="0">
                    <Value Enum="Seconds">0</Value>
                    <Value Enum="Minutes">1</Value>
                    <Value Enum="Hours">2</Value>
                    <Value Enum="Days">3</Value>
                    <Value Enum="Weeks">4</Value>
                  </DiscreteInfo>
                </Int>
              </ItemDefinitions>
            </Group>
          </ChildrenDefinitions>
          <DiscreteInfo DefaultIndex="0">
            <Structure>
              <Value Enum="Function">0</Value>
              <Items>
                <Item>OutputFunction</Item>
              </Items>
            </Structure>
            <Structure>
              <Value Enum="Auto-Build">1</Value>
              <Items>
                <Item>OutputUnits</Item>
                <Item>TimeSeriesData</Item>
              </Items>
            </Structure>
          </DiscreteInfo>
        </Int>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
  <!--**********  Attribute Instances ***********-->
  <Attributes />
  <!--********** Workflow Views ***********-->
  <Views>
    <View Type="Group" Title="SimBuilder" TopLevel="true">
      <DefaultColor>1., 1., 0.5, 1.</DefaultColor>
      <InvalidColor>1, 0.5, 0.5, 1</InvalidColor>
      <Views>
        <View Title="Solvers" />
        <View Title="Time" />
        <View Title="Globals" />
        <View Title="Functions" />
        <View Title="Materials" />
        <View Title="Constituents" />
        <View Title="Friction" />
        <View Title="BoundaryConditions" />
      </Views>
    </View>
    <View Type="Attribute" Title="BoundaryConditions" ModelEntityFilter="e">
      <AttributeTypes>
        <Att Type="VelocityBound" />
        <Att Type="LidElevation" />
        <Att Type="VelocityAndDepthBound" />
        <Att Type="WaterDepthLid" />
        <Att Type="WaterSurfElev" />
        <Att Type="UnitFlow" />
        <Att Type="TotalDischarge" />
        <Att Type="FloatingStationary" />
        <Att Type="DirichletTransport" />
        <Att Type="NaturalTransport" />
      </AttributeTypes>
    </View>
    <View Type="Attribute" Title="Constituents">
      <AttributeTypes>
        <Att Type="Constituent" />
      </AttributeTypes>
    </View>
    <View Type="Attribute" Title="Friction" CreateEntities="true" ModelEntityFilter="f">
      <AttributeTypes>
        <Att Type="Friction" />
      </AttributeTypes>
    </View>
    <View Type="SimpleExpression" Title="Functions">
      <Att Type="PolyLinearFunction" />
    </View>
    <View Type="Instanced" Title="Globals">
      <InstancedAttributes>
        <Att Name="Globals" Type="Globals" />
      </InstancedAttributes>
    </View>
    <View Type="Attribute" Title="Materials" CreateEntities="true" ModelEntityFilter="f">
      <AttributeTypes>
        <Att Type="SolidMaterial" />
      </AttributeTypes>
    </View>
    <View Type="Instanced" Title="Solvers">
      <InstancedAttributes>
        <Att Name="Solvers" Type="Solvers" />
      </InstancedAttributes>
    </View>
    <View Type="Instanced" Title="Time">
      <InstancedAttributes>
        <Att Name="Time" Type="Time" />
      </InstancedAttributes>
    </View>
  </Views>
</SMTK_AttributeSystem>
